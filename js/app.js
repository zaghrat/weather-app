const weatherIcons = {
    'Rain' : 'wi wi-day-rain',
    'Snow' : 'wi wi-day-snow',
    'Clouds': 'wi wi-day-cloudy',
    'Clear': 'wi wi-day-sunny',
    'Mist': 'wi wi-day-fog',
    'Haze': 'wi wi-day-fog',
    'Drizzle': 'wi wi-day-sleet',
};

function capitalize(str) {
    return str[0].toUpperCase() + str.slice(1);
}

async function main(withIP = true) {
    let city;
    if (withIP) {
        const ip = await fetch('https://api.ipify.org?format=json')
            .then(result => result.json())
            .then(json =>  json.ip);

        city = await fetch('http://api.ipstack.com/'+ip+'?access_key=9811ed0cf23000e0c6554148745ff90a&format=1')
            .then(result => result.json())
            .then(json => json.city);
    } else {
        city = document.querySelector('#city').textContent;
    }


    const weather = await fetch('https://api.openweathermap.org/data/2.5/weather?q='+city+'&lang=en&appid=32ba3729283de880b80081ae1db86a60&units=metric')
            .then(result => result.json())
            .then(json => json);

    displayData(weather);
}

function displayData(data)
{
    const city = data.name;
    const country = data.sys.country;
    const temperature = Math.ceil(data.main.temp).toString();
    const conditions = capitalize(data.weather[0].description);
    const icon = weatherIcons[data.weather[0].main];
    const bodyClass = data.weather[0].main.toLowerCase();
    const feels_like = Math.ceil(data.main.feels_like).toString();
    const humidity = data.main.humidity;

    document.querySelector('#city').textContent = city;
    document.querySelector('#country').textContent = country;
    document.querySelector('#feels_like').textContent = feels_like + 'C°';
    document.querySelector('#humidity').textContent = humidity;
    document.querySelector('.temperature').textContent = temperature;
    document.querySelector('#conditions').textContent = conditions;
    document.querySelector('#icon').className = icon;
    document.querySelector('body').className = bodyClass;
}

const city = document.querySelector('#city');
city.addEventListener('click',() => {
    city.contentEditable = true;
});


city.addEventListener('keydown', (e) => {
    if (e.keyCode == 13) {
        e.preventDefault();
        city.contentEditable = false;
        main(false);
    }
});

main();