<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Weather</title>
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/weather-icons.min.css">
</head>
<body>
    <section id="app">
        <h1>
            <span id="city"></span>
            <span id="country"></span>
            <span class="tooltip">click to type another city</span>
        </h1>

        <i id="icon"></i>

        <h2>
            <span id="conditions"></span>
            <span class="temperature"></span>
            C°
        </h2>

        <h3>
            Feels like <span id="feels_like"></span>
        </h3>
        <h3>
            Humidity <span id="humidity"></span>
        </h3>
    </section>

    <script src="js/app.js"></script>
</body>
</html>